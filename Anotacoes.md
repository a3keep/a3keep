
#29/05/2018
- @Marcilioth: Foi criado o repositório para o projeto A3-Keep, e configurado o grupo de permissões.


#Lista De Features

- criação, edição  e remoção de notas
- alinhamento das notas em linhas ou blocos; 
- alteração das cores de cada uma das notas; 
- instruções para iniciantes;
- busca de notas;
- sincronização automática com o servidor;
- organização através de tags;
- layout intuitivo
 


