import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AppComponent } from '../../app.component'

@Component({
  selector: 'cartoes',
  templateUrl: './cartoes.component.html',
  styleUrls: ['./cartoes.component.css']
})
export class CartoesComponent implements OnInit {
  
  ngOnInit(): void {
    this.obj = document.querySelector('.cartao').querySelector('p');
    this.obj.focus()
  }
  constructor($appComponent : AppComponent){
    this.$appComponent = $appComponent  
  }

  $appComponent : AppComponent
  @Input() _id
  @Input() conteudo
  @Input() cor
  @Input() usuario
  @Input() tags : Array<String>
  @Input() removerCartao
  @Output() buscaTag = new EventEmitter<String>()
  obj : any

  opcoesCartao = [
    {cor:'amarelo', codigo:'rgb(235, 239, 64)', class:'corPadrão-cartao'},
    {cor:'vermelho', codigo:'rgb(240, 84, 80)', class: 'corImportante-cartao'},
    {cor:'azul', codigo:'rgb(146, 196, 236)', class: 'corTarefa-cartao'},
    {cor:'verde', codigo:'rgb(118, 239, 64)', class: 'corInspiracao-cartao'}
  ];

  mudaCorCard(card, cor,p,tags) {
    card.style.backgroundColor = cor;
    this.$appComponent.salvarCartao(card,p, tags)
  }

  salvarTexto(card,p,tags) {
    this.$appComponent.salvarCartao(card,p, tags)
  }

  editarTag(cartao,tags){
    this.obj = {
      cartao: cartao,
      tags: tags
    }
    this.$appComponent.cartaoTags = this.obj;
    this.$appComponent.formBool = !this.$appComponent.formBool
  }

  addHover(elemento){
    elemento.querySelector('.opcoesDoCartao').classList.add('cartaoCriado')  
  }

  removeHover(elemento){
    elemento.querySelector('.opcoesDoCartao').classList.remove('cartaoCriado') 
  }
}
