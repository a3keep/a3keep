import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent {
  
  constructor($appComponent : AppComponent){
    this.$appComponent = $appComponent  
  }
  
  $appComponent: AppComponent
  @Input() tag
  
  removerTag = (tag,evento) => {
    if (evento.path[0].classList.contains('lixeira')) {
      this.$appComponent.removerTag(tag)
    }
  }
  
  buscaTag = (texto,evento) => {
    if (!evento.path[0].classList.contains('lixeira')) {
      this.$appComponent.mudarBuscaTag(texto)
    }
  }
}
