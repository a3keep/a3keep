import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ajudas',
  templateUrl: './ajudas.component.html',
  styleUrls: ['./ajudas.component.css']
})
export class AjudasComponent{
  @Input() listaAjuda
  @Input() onMudarAjuda
}
