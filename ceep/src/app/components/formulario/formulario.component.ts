import { Component, OnInit, Input } from '@angular/core';
import { CartoesComponent } from '../cartoes/cartoes.component';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  
  ngOnInit(): void {
    this.cartaoTags = this.$appComponent.cartaoTags.tags
  }
  constructor($appComponent : AppComponent){
    this.$appComponent = $appComponent  
  }

  tagsTeste : Array<String> = []
  @Input() salvarCartao
  @Input() onToggleForm
  selecionarAmarelo: boolean = true;
  selecionarVermelho: boolean = false;
  selecionarAzul: boolean = false;
  selecionarVerde: boolean = false;
  @Input() tags
  cartaoTags
  @Input() alimentaTag
  $appComponent : AppComponent

  addTag(tag){ 
    this.$appComponent.addTagCartao(tag.nome)
  } 
}
