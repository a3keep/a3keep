import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'cabecalho',
  templateUrl: './cabecalho.component.html',
  styleUrls: ['./cabecalho.component.css']
})
export class CabecalhoComponent {

  constructor($appComponent : AppComponent){
    this.$appComponent = $appComponent  
  }

  $appComponent : AppComponent
  @Input() titulo
  @Input() mudaLayout
  @Input() mural
  @Input() mudarAjuda  
  @Input() onToggleForm
  @Input() usuarios
  @Input() usuarioLogado
  @Output() busca = new EventEmitter<String>()

  mudarUsuarioLogado(usuario) {
    this.$appComponent.mudarUsuarioLogado(usuario)
  }

  addCartao(){
    this.$appComponent.addCartao()
  }
}
