import { Component, Inject, OnInit, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CartoesComponent } from './components/cartoes/cartoes.component';
import { TagsComponent } from './components/tags/tags.component';
import { timingSafeEqual } from 'crypto';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  muralLinha: boolean = false;
  titulo : String = 'Notes'
  
  cartoes: Array<any> = []
  tags: Array<any> = []
  tagsSalvar: Array<String> = []
  
  cartaoTags: {
    cartao: any,
    tags: any
  };
  
  buscaTag: String = ''
  busca: String =''
  
  ajudas: Array<String> = [
    'Você pode criar, modificar e excluir notas',
    'Você pode criar tags para as notas',
    'Para adicionar uma nota clique no botão +',
    'Para mudar o formato de exibição das notas clique no botão Linhas',
    'Esqueça o botão salvar, qualquer alteração é automaticamente sincronizada na nuvem',
    'Muito obrigado por utilizar o A³ Notes'
  ]
  
  ajudasBool: Boolean = false;
  formBool: Boolean = false;
  domRef: ElementRef
  $http: HttpClient
  
  $usuarios: Array<any> = [
    {
      usuario: 'fereinaux_',
      img: '../assets/img/fereinaux_.jfif'
    },{
      usuario: 'gildoso',
      img: '../assets/img/gildoso.jpg'
    },{
      usuario: 'jcarlos',
      img: '../assets/img/jcarlos.jpg'
    },{
      usuario: 'marcilioth',
      img: '../assets/img/marcilioth.png'
    },{
      usuario: 'danilima',
      img: '../assets/img/danilima.png'
    }
  ]
  
  $usuarioLogado = this.$usuarios[1]
  
  
  constructor($http: HttpClient){
    this.$http = $http
    this.listarCartoes()
    this.listarTags()
  }
  
  listarCartoes = () => {
    this.$http
    .get("http://localhost:3000/ceep/cartoes?usuario=" + this.$usuarioLogado.usuario)
    .subscribe((data : CartoesComponent[]) => {
      this.cartoes = data
    })
  }
  
  listarTags = () => {
    this.$http
    .get("http://localhost:3000/ceep/tags?usuario=" + this.$usuarioLogado.usuario)
    .subscribe((data : TagsComponent[]) => {
      this.tags = data
    }) 
  }
  
  salvarCartao = (elemento, conteudo,tags) => {
    tags == undefined ? tags = this.tagsSalvar : tags = tags
    const cartao = {
      conteudo: conteudo.textContent,
      cor: elemento.style.backgroundColor ? elemento.style.backgroundColor : 'rgb(235, 239, 64)',
      _id: conteudo.id,
      tags: tags,
      usuario: this.$usuarioLogado.usuario
    };
    let cabecalho = {headers: new HttpHeaders ({'Content-Type':'application/json'})}
    if (cartao._id == '') {
      this.$http
      .post("http://localhost:3000/ceep/cartoes",
      cartao,
      cabecalho)
      .subscribe((data) => {
        cartao._id = data
        this.cartoes = [...this.cartoes,cartao]
        // this.formBool = !this.formBool
      }) 
      
    } else {
      this.$http
      .put("http://localhost:3000/ceep/cartoes",
      cartao,
      cabecalho)
      .subscribe((data) => {
      }) 
    }
  }
  
  salvarTag = (tagObj) => {
    const tag = {
      nome: tagObj.value,
      _id: tagObj.id,
      usuario: this.$usuarioLogado.usuario
    };
    let cabecalho = {headers: new HttpHeaders ({'Content-Type':'application/json'})}
    this.$http
    .post("http://localhost:3000/ceep/tags",
    tag,
    cabecalho)
    .subscribe((data) => {
      tag._id = data
      tagObj.value = '';
      this.tags = [...this.tags,tag]
    })
  }
  
  removerCartao = (_id) => {
    let confirma = confirm("Deseja excluir este cartão?");
    if(confirma) {
      this.$http
      .delete("http://localhost:3000/ceep/cartoes?_id="+_id)
      .subscribe(() => {
        this.cartoes = this.cartoes.filter(cartao => _id !== cartao._id)
      })
    }
  }
  
  removerTag = (tagRemover) => {
    let cabecalho = {headers: new HttpHeaders ({'Content-Type':'application/json'})}
    this.$http
    .delete("http://localhost:3000/ceep/tags?_id="+tagRemover._id)
    .subscribe(() => {
      this.tags = this.tags.filter(tag => tagRemover._id !== tag._id)
      for (let i = 0; i < this.cartoes.length; i++) {
        const cartao = this.cartoes[i];
        cartao.tags = cartao.tags.filter((tag) => tagRemover.nome != tag)
        this.$http
        .put("http://localhost:3000/ceep/cartoes", 
        cartao,
        cabecalho)
        .subscribe((data) => {
        }) 
      } 
    })
  }
  
  mudaLayout = (mural, botao) => {
    this.muralLinha = !this.muralLinha;
    botao.textContent.trim() == 'Linhas'
    ? botao.textContent = 'Blocos'
    : botao.textContent = 'Linhas'
  }
  
  mudarAjuda = () => {
    this.ajudasBool = !this.ajudasBool
    // this.formBool = false;
  }
  
  mudarForm = () => {
    this.formBool = !this.formBool
    this.tagsSalvar = []
    this.ajudasBool = false;
  }
  
  addCartao = () => {
    const cartao = {
      conteudo: '',
      cor: 'rgb(235, 239, 64)',
      usuario: this.$usuarioLogado.usuario,
      _id: undefined,
      tags: []
    }
    let cabecalho = {headers: new HttpHeaders ({'Content-Type':'application/json'})}
    this.$http
    .post("http://localhost:3000/ceep/cartoes",
    cartao,
    cabecalho)
    .subscribe((data) => {
      cartao._id = data
      this.cartoes = [cartao,...this.cartoes]
    })
  }
  
  mudarBusca = (busca: String) => {
    this.busca = busca
  }
  
  mudarBuscaTag = (buscaTag : String) => {
    this.buscaTag == buscaTag ? this.buscaTag = '' : this.buscaTag = buscaTag
  }
  
  mudarUsuarioLogado = (usuario) => {
    this.$usuarioLogado = usuario;
    this.listarCartoes()
    this.listarTags()
  }
  
  closeModal = () =>{
    this.ajudasBool = false;
    this.formBool = false;
  }
  
  alimentaTag = (tag) => {
    this.tagsSalvar.includes(tag) 
    ? this.tagsSalvar = this.tagsSalvar.filter(tagLocal => tagLocal != tag) 
    : this.tagsSalvar.push(tag)
  }
  
  addTagCartao = (tag) => {
    const i = this.cartaoTags.tags.indexOf(tag)
    if (i > -1){
      this.cartaoTags.tags.splice(i,1)
      this.cartaoTags.tags.filter((tagLocal) => tagLocal !== tag) 
    }
    else
    {
      this.cartaoTags.tags.push(tag)
    }
    
    const conteudo = this.cartaoTags.cartao.querySelector('p')
    this.salvarCartao(this.cartaoTags.cartao,conteudo,this.cartaoTags.tags)
  }
}