import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { CabecalhoComponent } from './components/cabecalho/cabecalho.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { CartoesComponent } from './components/cartoes/cartoes.component';
import { AjudasComponent } from './components/ajudas/ajudas.component';
import { TagsComponent } from './components/tags/tags.component';
import { FiltroPipe } from './pipes/filtro.pipe';
import { FiltroTagPipe } from './pipes/filtro-tag.pipe';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    CabecalhoComponent,
    FormularioComponent,
    CartoesComponent,
    AjudasComponent,
    TagsComponent,
    FiltroPipe,
    FiltroTagPipe
  ],
  imports: [
    BrowserModule,
    CommonModule, 
    HttpClientModule,
    BsDropdownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
