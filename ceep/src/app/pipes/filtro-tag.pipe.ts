import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroTag'
})
export class FiltroTagPipe implements PipeTransform {

  transform(lista: Array<any>, tag : string = ''): any {
    return lista.filter((cartao) => tag == '' ? lista : cartao.tags.includes(tag.trim()));
  }

}
