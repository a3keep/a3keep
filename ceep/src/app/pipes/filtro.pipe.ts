import { Pipe, PipeTransform } from '@angular/core';
import {CartoesComponent} from '../components/cartoes/cartoes.component'

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(lista: Array<CartoesComponent>, conteudo : string = ''): any {
    return lista.filter((cartao) => cartao.conteudo.toLowerCase().includes(conteudo.toLowerCase()));
  }

}
