var dbCartoes = require('../../config/dbCartoes');
var dbTags = require('../../config/dbTags');

var api = {}

api.adicionaCartao = function(req, res) {
    var cartao = req.body;
    delete cartao._id;
    dbCartoes.insert(cartao, function(err, newDoc) {
        if(err) return console.log(err);
        console.log('Adicionado com sucesso: ' + newDoc._id);
        res.json(newDoc._id);
    });  
};

api.adicionaTag = function(req, res) {
    var tag = req.body;
    delete tag._id;
    dbTags.insert(tag, function(err, newDoc) {
        if(err) return console.log(err);
        console.log('Adicionado com sucesso: ' + newDoc._id);
        res.json(newDoc._id);
    });  
};

api.busca = function(req, res) {
   db.findOne({_id: req.params.fotoId }, function(err, doc) {
        if (err) return console.log(err);
        res.json(doc);
    });
};

api.listaCartao = function(req, res) {
    dbCartoes.find(req.query).sort({titulo: 1}).exec(function(err, doc) {
        if (err) return console.log(err);
        res.json(doc);
    });
};

api.listaTag = function(req, res) {
    dbTags.find(req.query).sort({titulo: 1}).exec(function(err, doc) {
        if (err) return console.log(err);
        res.json(doc);
    });
};

api.editaCartao = function(req, res) {
    console.log(req.body)
    dbCartoes.update({_id: req.body._id},req.body, {}, function (err, numRemoved) {
        if (err) return console.log(err);
        console.log('editado com sucesso');
        if(numRemoved) res.status(200).end();
        res.status(500).end();
    });
};

api.removeCartao = function(req, res) {
    dbCartoes.remove(req.query, {}, function (err, numRemoved) {
        if (err) return console.log(err);
        console.log('removido com sucesso');
        console.log(numRemoved)
        if(numRemoved) res.status(200).end();
        res.status(500).end();
    });
};

api.editaTag = function(req, res) {
    dbTag.remove(req.query, {}, function (err, numRemoved) {
        if (err) return console.log(err);
        console.log('removido com sucesso');
        console.log(numRemoved)
        if(numRemoved) res.status(200).end();
        res.status(500).end();
    });
};

api.removeTag = function(req, res) {
    console.log(req.query)
    dbTags.remove(req.query, {}, function (err, numRemoved) {
        if (err) return console.log(err);
        console.log('removido com sucesso');
        console.log(numRemoved)
        if(numRemoved) res.status(200).end();
        res.status(500).end();
    });
};


module.exports = api;