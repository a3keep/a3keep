var api = require('../api'),
path = require('path');

module.exports  = function(app) {

app.route('/ceep/cartoes')
    .post(api.adicionaCartao)
    .put(api.editaCartao)
    .get(api.listaCartao)
    .delete(api.removeCartao);

app.route('/ceep/tags')
    .post(api.adicionaTag)
    .put(api.editaTag)
    .get(api.listaTag)
    .delete(api.removeTag);
    
app.all('/*', function(req, res) {
    res.sendFile(path.join(app.get('clientPath'), 'index.html'));
});
};

