var Datastore = require('nedb')
,dbTagName = 'tags.db',
dbTag;


if(!dbTag) {
    dbTag = new Datastore({
        filename: dbTagName, 
        autoload: true 
    });
    console.log('Banco ' + dbTagName + ' pronto para uso')
}

module.exports = dbTag;