var Datastore = require('nedb')
,dbCartoesName = 'cartoes.db'
,dbCartoes;

if(!dbCartoes) {
    dbCartoes = new Datastore({
        filename: dbCartoesName, 
        autoload: true 
    });
    console.log('Banco ' + dbCartoesName + ' pronto para uso')
}

module.exports = dbCartoes;